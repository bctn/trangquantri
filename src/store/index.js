import Vue from "vue";
import Vuex from "vuex";

import UnitAddModule from "../store/modules/unit_add";
import TeamAddModule from "../store/modules/team_add";
import StaffAddModule from "../store/modules/staff_add";
import NewsAddModule from "../store/modules/news_add";
import AttendanceAddModule from "../store/modules/attendance_add";
import UserAddModule from "../store/modules/user_add";

// OPTIONS
import UnitAcModule from "../store/modules/options/unit_ac";
import TeamAcModule from "../store/modules/options/team_ac";
import StaffAcModule from "./modules/options/staff_ac";
import AuthModule from "./modules/auth.js";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth: AuthModule,

    unit_add: UnitAddModule,
    team_add: TeamAddModule,
    staff_add: StaffAddModule,
    news_add: NewsAddModule,
    attendance_add: AttendanceAddModule,
    user_add: UserAddModule,

    // OPTIONS
    unit_ac: UnitAcModule,
    team_ac: TeamAcModule,
    staff_ac: StaffAcModule
  }
});
