import NewsService from "../../services/news";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    id: "",
    title: "",
    content: "",
    category_id: "",
    thumbnails: "",
    source: ""
  }
};

const NewsAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },
    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        title: state.basicInfo.title,
        content: state.basicInfo.content,
        category_id: state.basicInfo.category_id,
        thumbnails: state.basicInfo.thumbnails,
        source: state.basicInfo.source
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        NewsService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.title = basicInfo.title;
      state.basicInfo.content = basicInfo.content;
      state.basicInfo.category_id = basicInfo.category_id;
      state.basicInfo.thumbnails = basicInfo.thumbnails;
      state.basicInfo.source = basicInfo.source;
    },

    reset(state) {
      state.basicInfo.name = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default NewsAddModule;
