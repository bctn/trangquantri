import UserService from "../../services/user";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    username: "",
    password: "",
    code_number_staff: "",
    role: "",
    status: ""
  }
};

const UserAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        username: state.basicInfo.username,
        password: state.basicInfo.password,
        code_number_staff: state.basicInfo.code_number_staff,
        role: state.basicInfo.role,
        status: state.basicInfo.status
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        UserService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.username = basicInfo.username;
      state.basicInfo.password = basicInfo.password;
      state.basicInfo.code_number_staff = basicInfo.code_number_staff;
      state.basicInfo.role = basicInfo.role;
      state.basicInfo.status = basicInfo.status;
    },

    reset(state) {
      state.basicInfo.username = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default UserAddModule;
