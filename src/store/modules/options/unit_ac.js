import AcService from "../../../services/unit_ac";
// import * as _ from "lodash";

const CategoryModule = {
  namespaced: true,
  state: {
    isLoading: false,

    unitOptions: [],
    teamOptions: [],
    staffOptions: []
  },
  actions: {
    loadUnitData({ commit }) {
      commit("onLoadUnitData");
      return new Promise((resolve, reject) => {
        AcService.getDepartments()
          .then(resp => {
            // console.log(resp);
            commit("onProcessingUnitData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingUnitDataFailure");
            reject(err);
          });
      });
    },

    changeUnit({ commit }, departmentId) {
      return new Promise((resolve, reject) => {
        localStorage.removeItem("departmentId");
        localStorage.setItem("departmentId", departmentId);
        AcService.getClassrooms(departmentId)
          .then(resp => {
            commit("onProcessingTeamData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadUnitData(state) {
      state.isLoading = true;
    },
    onProcessingUnitData(state, data) {
      state.isLoading = false;

      state.unitOptions = [{ value: 0, text: "--- Chọn Phòng ban ---" }];
      state.teamOptions = [{ value: 0, text: "--- Chọn độn, đồn, tổ ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.unitOptions.push({
          value: data[i]["code_number_unit"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingUnitDataFailure(state) {
      state.isLoading = false;
    },

    onProcessingTeamData(state, data) {
      state.districts = data;

      state.teamOptions = [{ value: 0, text: "--- Chọn đội, đồn, tổ ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.teamOptions.push({
          value: data[i]["code_number_team"],
          text: data[i]["name"]
        });
      }
    },

    onProcessingTeacherData(state, data) {
      state.districts = data;

      state.staffOptions = [{ value: 0, text: "--- Chọn cán bộ ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.staffOptions.push({
          value: data[i]["code_number_staff"],
          text: data[i]["code_number_staff"]
        });
      }
    }
  }
};

export default CategoryModule;
