import TeamAcService from "../../../services/team";

const TeamAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    teamOptions: []
  },
  actions: {
    loadTeamData({ commit }) {
      commit("onLoadClassroomData");
      return new Promise((resolve, reject) => {
          TeamAcService.getList()
          .then(resp => {
            console.log(resp);
            commit("onProcessingClassroomData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingClassroomDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadClassroomData(state) {
      state.isLoading = true;
    },
    onProcessingClassroomData(state, data) {
      state.isLoading = false;

      state.teamOptions = [{ value: 0, text: "--- Chọn đội, đồn, tổ ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.teamOptions.push({
          value: data[i]["code_number_team"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingClassroomDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default TeamAcModule;
