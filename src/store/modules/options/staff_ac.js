import StaffAcService from "../../../services/staff";

const StaffAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    staffOptions: []
  },
  actions: {
    loadStaffData({ commit }) {
      commit("onLoadStaffData");
      return new Promise((resolve, reject) => {
        StaffAcService.getAll()
          .then(resp => {
            console.log(resp);
            commit("onProcessingStaffData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingStaffDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadStaffData(state) {
      state.isLoading = true;
    },
    onProcessingStaffData(state, data) {
      state.isLoading = false;

      state.staffOptions = [{ value: 0, text: "--- Chọn Cán bộ ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.staffOptions.push({
          value: data[i]["code_number_staff"],
          text: data[i]["last_name"] + " " + data[i]["first_name"]
        });
      }
    },
    onProcessingStaffDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default StaffAcModule;
