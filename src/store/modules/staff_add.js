import StaffService from "../../services/staff";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    id: "",
    code_number_staff: "",
    slug: "",
    last_name: "",
    first_name: "",
    sex: "",
    birthday: "",
    thumbnails: "",
    email: "",
    email_verified_at: "",
    identity_number: "",
    phone: "",
    mobile: "",
    mobile_verified_at: "",
    place_of_birth: "",
    home_town: "",
    permanent_residence: "",
    current_home: "",
    nation: "",
    religion: "",
    nationality: "",
    level: "",
    specialized: "",

    position_staff: "",
    type_staff: "",
    clan: "",
    date_clan: "",

    code_number_unit: "",
    code_number_team: ""
  }
};

const StaffAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        code_number_staff: state.basicInfo.code_number_staff,
        last_name: state.basicInfo.last_name,
        first_name: state.basicInfo.first_name,
        sex: state.basicInfo.sex,
        birthday: state.basicInfo.birthday,
        thumbnails: state.basicInfo.thumbnails,
        email: state.basicInfo.email,
        identity_number: state.basicInfo.identity_number,
        phone: state.basicInfo.phone,
        mobile: state.basicInfo.mobile,
        place_of_birth: state.basicInfo.place_of_birth,
        home_town: state.basicInfo.home_town,
        permanent_residence: state.basicInfo.permanent_residence,
        current_home: state.basicInfo.current_home,
        nation: state.basicInfo.nation,
        religion: state.basicInfo.religion,
        nationality: state.basicInfo.nationality,
        level: state.basicInfo.level,
        specialized: state.basicInfo.specialized,

        position_staff: state.basicInfo.position_staff,
        type_staff: state.basicInfo.type_staff,
        clan: state.basicInfo.clan,
        date_clan: state.basicInfo.date_clan,
        code_number_unit: state.basicInfo.code_number_unit,
        code_number_team: state.basicInfo.code_number_team
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        StaffService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.code_number_staff = basicInfo.code_number_staff;
      state.basicInfo.last_name = basicInfo.last_name;
      state.basicInfo.first_name = basicInfo.first_name;
      state.basicInfo.sex = basicInfo.sex;
      state.basicInfo.birthday = basicInfo.birthday;
      state.basicInfo.thumbnails = basicInfo.thumbnails;
      state.basicInfo.email = basicInfo.email;
      state.basicInfo.identity_number = basicInfo.identity_number;
      state.basicInfo.phone = basicInfo.phone;
      state.basicInfo.mobile = basicInfo.mobile;
      state.basicInfo.place_of_birth = basicInfo.place_of_birth;
      state.basicInfo.home_town = basicInfo.home_town;
      state.basicInfo.permanent_residence = basicInfo.permanent_residence;
      state.basicInfo.current_home = basicInfo.current_home;
      state.basicInfo.nation = basicInfo.nation;
      state.basicInfo.religion = basicInfo.religion;
      state.basicInfo.nationality = basicInfo.nationality;
      state.basicInfo.level = basicInfo.level;
      state.basicInfo.specialized = basicInfo.specialized;

      state.basicInfo.position_staff = basicInfo.position_staff;
      state.basicInfo.type_staff = basicInfo.type_staff;
      state.basicInfo.clan = basicInfo.clan;
      state.basicInfo.date_clan = basicInfo.date_clan;
      state.basicInfo.code_number_unit = basicInfo.code_number_unit;
      state.basicInfo.code_number_team = basicInfo.code_number_team;
    },

    reset(state) {
      state.basicInfo.code_number_staff = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default StaffAddModule;
