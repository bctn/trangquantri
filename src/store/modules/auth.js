// import axios from 'axios'
import axios from "../../axios";

const AuthModule = {
  namespaced: true,
  state: {
    isLoggedIn: false,
    userDetails: {}
  },
  getters: {
    loggedIn(state) {
      return state.isLoggedIn;
    },
    setUserDetails(state) {
      return state.userDetails;
    }
  },
  mutations: {
    setLoggedIn(state, payload) {
      state.isLoggedIn = payload;
    },
    setUserDetails(state, payload) {
      state.userDetails = payload;
    }
  },
  actions: {
    loginUser(ctx, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post("auth/login", payload)
          .then(response => {
              console.log(response);
            if (response.data.access_token) {
              localStorage.setItem("token", response.data.access_token);
              ctx.commit("setLoggedIn", true);
            } else {
              console.log("Phiên làm việc kết thúc!");
            }
            resolve(response);
          })
          .catch(error => {
            reject(error);
          });
      });
    },

    logoutUser(ctx) {
      return new Promise(resolve => {
        localStorage.removeItem("token");
        ctx.commit("setLoggedIn", false);
        resolve(true);
      });
    },
    setLoggedInState(ctx) {
      return new Promise(resolve => {
        if (localStorage.getItem("token")) {
          ctx.commit("setLoggedIn", true);
          resolve(true);
        } else {
          ctx.commit("setLoggedIn", false);
          resolve(false);
        }
      });
    },
    me(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("auth/me")
          .then(response => {
            if (response.data.active === 1) {
              ctx.commit("setUserDetails", response.data);
              resolve(response);
            } else {
              localStorage.removeItem("token");
              ctx.commit("setLoggedIn", false);
              this.$router.push("/login");
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    }
  }
};

export default AuthModule;
