import AttendanceService from "../../services/attendance";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    id: "",
    code_number_team: "",
    code_number_staff: "",
    date_absent: "",
    absent: 0,
    reason_for_absence: ""
  }
};

const AttendanceAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        code_number_team: state.basicInfo.code_number_team,
        code_number_staff: state.basicInfo.code_number_staff,
        date_absent: state.basicInfo.date_absent,
        absent: state.basicInfo.absent,
        reason_for_absence: state.basicInfo.reason_for_absence
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        AttendanceService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.code_number_team = basicInfo.code_number_team;
      state.basicInfo.code_number_staff = basicInfo.code_number_staff;
      state.basicInfo.date_absent = basicInfo.date_absent;
      state.basicInfo.absent = basicInfo.absent;
      state.basicInfo.reason_for_absence = basicInfo.reason_for_absence;
    },

    reset(state) {
      console.log('ok đã vào');
      state.basicInfo.code_number_team = "";
      state.basicInfo.code_number_staff = "";
      state.basicInfo.date_absent = "";
      state.basicInfo.absent = "";
      state.basicInfo.reason_for_absence = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo,
    reset: state => state.reset
  }
};

export default AttendanceAddModule;
