import Router from "vue-router";
// import auth from "../services/auth";
import Middlewares from "../middlewares/index";

import DashView from "../components/Dash.vue";

import LoginView from "../views/auth/Login.vue";
import NotFoundView from "../components/404.vue";

// HIỂN THỊ LỖI TRANG
import Index404 from "../views/error/index404";

// Team
import Team from "../views/team/index";
import TeamEdit from "../views/team/edit";
import TeamAdd from "../views/team/add";

// Staff
import Staff from "../views/staff/index";
import StaffEdit from "../views/staff/edit";
import StaffAdd from "../views/staff/add";

// News
import News from "../views/news/index";
import NewsEdit from "../views/news/edit";
import NewsAdd from "../views/news/add";

// Unit
import Unit from "../views/unit/index";
import UnitEdit from "../views/unit/edit";
import UnitAdd from "../views/unit/add";

// Attendance
import Attendance from "../views/attendance/index";
import AttendanceEdit from "../views/attendance/edit";
import AttendanceAdd from "../views/attendance/add";

import Vue from "vue";

// User
import User from "../views/user/index";
import UserEdit from "../views/user/edit";
import UserAdd from "../views/user/add";

// Contact
import Contact from "../views/contact/index";

//statistical
import Statistical from "../views/statistical/index";

Vue.use(Router);

// Routes
const router = new Router({
  linkExactActiveClass: "active", // link active
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/login",
      component: LoginView,
      meta: {
        middleware: [Middlewares.guest]
      }
    },
    {
      path: "/",
      component: DashView,
      children: [
        // Team
        {
          path: "/team",
          component: Team,
          name: "index",
          meta: {
            middleware: [Middlewares.auth]
          }
        },
        {
          path: "/team/add",
          component: TeamAdd,
          name: "add",
          meta: {
            middleware: [Middlewares.auth]
          }
        },
        {
          path: "/team/edit/:id",
          component: TeamEdit,
          name: "team.edit",
          meta: {
            middleware: [Middlewares.auth]
          }
        },
        // Unit
        {
          path: "/unit",
          component: Unit,
          name: "index",
          meta: {
            middleware: [Middlewares.auth]
          }
        },
        {
          path: "/unit/add",
          component: UnitAdd,
          name: "add",
          meta: {
            middleware: [Middlewares.auth]
          }
        },
        {
          path: "/unit/edit/:id",
          component: UnitEdit,
          name: "unit.edit",
          meta: {
            middleware: [Middlewares.auth]
          }
        },
        // attendance
        {
          path: "/attendance",
          component: Attendance,
          name: "index",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        {
          path: "/attendance/add",
          component: AttendanceAdd,
          name: "add",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        {
          path: "/attendance/edit/:id",
          component: AttendanceEdit,
          name: "attendance.edit",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        // Teacher
        {
          path: "/staff",
          component: Staff,
          name: "staff.index",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        {
          path: "/staff/add",
          component: StaffAdd,
          name: "staff.add",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        {
          path: "/staff/edit/:id",
          component: StaffEdit,
          name: "staff.edit",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        // News
        {
          path: "/news",
          component: News,
          name: "index",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        {
          path: "/news/add",
          component: NewsAdd,
          name: "add",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        {
          path: "/news/edit/:id",
          component: NewsEdit,
          name: "news.edit",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        // User
        {
          path: "/user",
          component: User,
          name: "index",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        {
          path: "/user/add",
          component: UserAdd,
          name: "add",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        {
          path: "/user/edit/:id",
          component: UserEdit,
          name: "user.edit",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        // Contact
        {
          path: "/contact",
          component: Contact,
          name: "index",
          meta: {
            middleware: [Middlewares.auth]
          }        },
        // Statistical
        {
          path: "/statistical",
          component: Statistical,
          name: "index",
          meta: {
            middleware: [Middlewares.auth]
          } 
        },
        // Hiển thị lỗi trang 404 (Không tìm thấy)
        {
          path: "/index404",
          name: "index404.index",
          component: Index404
        }
      ]
    },
    {
      // not found handler
      path: "*",
      component: NotFoundView
    }
  ]
});

// router.beforeEach((to, from, next) => {
//   if (to.matched.some(record => record.meta.requiresAuth)) {
//     if (!auth.isLoggedIn()) {
//       next({
//         path: "/login",
//         query: { redirect: to.fullPath }
//       });
//     } else {
//       next();
//     }
//   } else {
//     next(); // make sure to always call next()!
//   }
// });

function nextCheck(context, middleware, index) {
  const nextMiddleware = middleware[index];
  if (!nextMiddleware) return context.next;
  return (...parameters) => {
    context.next(...parameters);
    const nextMidd = nextCheck(context, middleware, index + 1);

    nextMiddleware({ ...context, next: nextMidd });
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware];
    const ctx = {
      from,
      next,
      router,
      to
    };
    const nextMiddleware = nextCheck(ctx, middleware, 1);
    return middleware[0]({ ...ctx, next: nextMiddleware });
  }
  return next();
});


export default router;
