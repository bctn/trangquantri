import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// import Axios from "axios";

import CKEditor from "@ckeditor/ckeditor5-vue";
Vue.use(CKEditor);

Vue.config.productionTip = false;

// Axios.defaults.withCredentials = true;
// Axios.defaults.baseURL = "http://app.airlock-example.test:8000/"; // Đường dẫn đầu tiên + service api
// Axios.defaults.baseURL = "http://127.0.0.1:8000/"; // Đường dẫn đầu tiên + service api

new Vue({
  el: "#root",
  router,
  store,
  render: h => h(App)
}).$mount("#app");
