export default function guest({ next, router }) {
    if (localStorage.getItem("token")) {
      console.log("////////////////////////////////////");
      console.log(localStorage.getItem("token"));
      return router.push({ name: "home.index" });
    }
    return next();
  }
  