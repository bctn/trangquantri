import axios from "../axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getList() {
  return axios.get("team/getList");
}

function getAll() {
  return axios.get("team/list");
}

function getPaginate(page, perPage, sortColumn, direction) {
  return axios.get(
    "team/getPaginate?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("team/create", data);
}

function show(id) {
  return axios.get("team/show/" + id);
}

function update(id, data) {
  return axios.post("team/update/" + id, data);
}

function remove(id) {
  return axios.delete("team/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "team/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getList,
  getAll,
  getPaginate,
  submit,
  show,
  update,
  remove,
  getSearch
};
