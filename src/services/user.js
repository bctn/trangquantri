import axios from "../axios";
/**
 * Tạo user mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("user/list");
}

function getPaginate(page, perPage, sortColumn, direction) {
  return axios.get(
    "user/getPaginate?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("user/create", data);
}

function show(id) {
  return axios.get("user/show/" + id);
}

function update(id, data) {
  return axios.post("user/update/" + id, data);
}

function remove(id) {
  return axios.delete("user/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "user/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  show,
  update,
  getPaginate,
  submit,
  remove,
  getSearch
};
