import axios from "../axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("staff/list");
}

function getPaginate(page, perPage, sortColumn, direction) {
  return axios.get(
    "staff/getPaginate?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("staff/create", data);
}

function show(id) {
  return axios.get("staff/show/" + id);
}

function update(id, data) {
  return axios.post("staff/update/" + id, data);
}

function remove(id) {
  return axios.delete("staff/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "staff/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

function upload(formData, config) {
  return axios.post("staff/upload", formData, config);
}

export default {
  getAll,
  getPaginate,
  submit,
  show,
  update,
  remove,
  getSearch,
  upload
};
