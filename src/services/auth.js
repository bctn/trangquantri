import axios from "../axios";

export function logout() {
  return axios.get("auth/logout");
}
