import axios from "../axios";

function getDepartments() {
  return axios.get("unit/list");
}

function getDepartment(id) {
  return axios.get("unit/info/" + id);
}

function getClassrooms(id) {
  return axios.get("unit/getClassrooms/" + id);
}

function getSubjects(id) {
  return axios.get("unit/getSubjects/" + id);
}

function getClassroomBundle(id) {
  return axios.get("getClassroom/bundle/" + id);
}

function getSubjectBundle(departmentId, schoolYearId) {
  return axios.get(
    "department/getSubjects/bundle/" + departmentId + "/" + schoolYearId
  );
}

function getTeacher(id) {
  return axios.get("unit/getTeacher/" + id);
}

export default {
  getDepartments,
  getDepartment,
  getClassrooms,
  getSubjects,
  getClassroomBundle,
  getSubjectBundle,

  getTeacher
};
