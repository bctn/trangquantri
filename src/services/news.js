import axios from "../axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "news/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("news/create", data);
}

function show(id) {
  return axios.get("news/show/" + id);
}

function update(id, data) {
  return axios.post("news/update/" + id, data);
}

function remove(id) {
  return axios.delete("news/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "news/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

function upload(formData, config) {
  return axios.post("news/upload", formData, config);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  getSearch,
  upload
};
